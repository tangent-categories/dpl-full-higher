# Rebuild the cf file with the correct options and then build the project
cd src
bnfc --haskell -d --text-token --functor Language.cf
# go into the Language folder
cd Language
# TODO: do a similar check to see if happy is installed
happy --info=grammar-ambigs.txt --outfile=grammar.out Par.y
# remove the grammar.out file; it's not needed
rm grammar.out
# leave the directory
cd ..
# we're now back in the src directory
# let's move the grammar-ambigs to the project root 
mv Language/grammar-ambigs.txt ../grammar-ambigs.txt

# Remove the unused file that also breaks the build
rm Language/Test.hs

# Insert similar stuff for building llvm, grin, z3, etc -- i.e. non-haskell tools needed.  Eventually 
# figure out how to build them without registering and pass the arguments in as needed

# Go to the prjoect root and build the project
cd ..
stack build
