module Main where

import Language.Par (myLexer,pTerm)
import Language.Lex (mkPosToken,prToken,Token (..))
import Language.Print (printTree)
import Data.Text ()
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

import System.IO (hFlush,stdout)

showPosToken :: (Show a,Show b,Show c) => ((a,b),c) -> String
showPosToken ((l,c),s) = concat [show l, ":" , show c, "\t", show s]


main :: IO ()
main = do 
    TIO.putStrLn (T.pack "Enter term")
    hFlush stdout
    term <- TIO.getLine
    let tokens = myLexer term 
    case pTerm tokens of 
        Left s -> do
            putStrLn $ concatMap ( (++ "\n") . showPosToken . mkPosToken ) tokens
            putStrLn s
        Right termParsed -> putStrLn $ printTree termParsed
